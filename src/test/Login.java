package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Utilities.Configuration;
import Utilities.ConstantsLogin;
import Utilities.CrossBrowser;
import layout.LoginPOM;

public class Login extends CrossBrowser {


	//WebDriver driver;
		@BeforeTest
		public void setup() {
//			CrossBrowser crossBrowser = new CrossBrowser();
//			driver = crossBrowser.Browser("chrome");
			
			System.setProperty("webdriver.chrome.driver", Configuration.GoogleChrome);
			driver = new ChromeDriver();
			
			driver.manage().deleteAllCookies();
			driver.get("http://nexusinsuranceserver//chubbqa/Default.aspx");
			
			driver.manage().window().maximize();
		}
		
		@Test(priority = 0, description = "User must log in to CHUBB system successfully.")
		public void ChubbLoginPage() {
			LoginPOM login = new LoginPOM(driver);
			
			try {
				login.username(ConstantsLogin.USERNAME);
				Thread.sleep(5000);
				
				login.password(ConstantsLogin.PASSWORD);
				Thread.sleep(5000);
				
				login.loginButton();
				Thread.sleep(5000);
				
				login.ReadAndAgreeButton();
				Thread.sleep(5000);
			}
			catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		@AfterMethod
		public void TearDown() {
			driver.close();
			driver.quit();
		}
	}


