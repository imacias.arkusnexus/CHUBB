package Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Parameters;

public class CrossBrowser extends Configuration{
	
	public static WebDriver driver;

/*	static {
		driver = Browser("chrome");
	}*/
	
	@Parameters("browser")
	public static WebDriver Browser(String browser){
		try {
			//Select a browser.
			switch(browser) {
				case "chrome":
					System.setProperty("webdriver.chrome.driver", Configuration.GoogleChrome);
					driver = new ChromeDriver();
				break;
					
				case "mozilla":
					System.setProperty("webdriver.gecko.driver", Configuration.MozillaFirefox);
					driver = new FirefoxDriver();
					break;
					
				case "opera":
				break;
				
				default:
					System.out.println("Doesn't exist the browser: " + browser);
				break;	
			}
		}//Try
		
		catch(Exception ex) {
			System.out.println(ex.getMessage());
		}//Catch
		
		return driver;
		
	}//method.

}
