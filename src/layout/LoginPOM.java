package layout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPOM {

	private WebDriver driver;

	/*Constructor*/
	public LoginPOM(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	/*Controllers*/
	private By username = 
			By.name("usr_nm");
	private WebElement password = 
			driver.findElement(By.name("usr_pw"));
	private By loginButton = 
			By.id("btnLogin");
	
	private WebElement readAndAgreeButton = 
			driver.findElement(By.id("btn_accept"));
	
	/*Methods*/
	public void username(String Username) {
		driver.findElement(username).sendKeys(Username);;
	}
	
	public void password(String Password) {
		password.sendKeys(Password);
	}
	
	public void loginButton() {
		driver.findElement(loginButton).click();;
	}
	
	public void ReadAndAgreeButton() {
		readAndAgreeButton.click();
	}
}
