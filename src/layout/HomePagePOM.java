package layout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePagePOM {

	private WebDriver driver;
	
	/*Constructor*/
	public HomePagePOM(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	/*Controller*/
	private WebElement touristAutoLink = 
			driver.findElement(By.xpath("//*[@id=\"tbl_imgLinks\"]/tbody/tr[1]/td/a"));
	private WebElement rcUsaLink = 
			driver.findElement(By.xpath("//td/a[@href=\"./Operations/setup_callCenterRCSale.aspx?proc_id=105&act_id=226\"]"));
	private WebElement blanketLink = 
			driver.findElement(By.xpath("//td/a[@href='./Operations/Blanket.aspx?proc_id=112&act_id=235']"));
	
	/*Method*/
	public void ClickOnTouristAutoEmissor() {
		touristAutoLink.click();
	}
	
	public void ClickOnRcUsaEmissor() {
		rcUsaLink.click();
	}
	
	public void ClickOnBlanketLink() {
		blanketLink.click();
	}
}
